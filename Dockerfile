FROM prom/prometheus
ADD prometheus.yml /etc/prometheus/
ADD backend_url /etc/prometheus/backend_url
RUN export URL="$(cat</etc/prometheus/backend_url)"; sed "s/MONITOR_URL/$URL/g" /etc/prometheus/prometheus.yml > /etc/prometheus/prometheus.yml.changed && mv /etc/prometheus/prometheus.yml.changed /etc/prometheus/prometheus.yml

USER root

RUN chgrp -R 0 /prometheus && \
    chmod -R g+wx /prometheus
